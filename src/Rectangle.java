public class Rectangle {
    public int sideA;
    public int sideB;
    public Point topLeft;
    public int corner1x;
    public int corner1y;
    public int corner2x;
    public int corner2y;
    public int corner3x;
    public int corner3y;

    public Rectangle(int sideA, int sideB, Point topLeft) {
        this.sideA = sideA;
        this.sideB = sideB;
        this.topLeft = topLeft;
    }


    public int area() {
        return sideA*sideB;
    }
    public int perimeter(){
        return 2*(sideA+sideB);
    }

    public int[][] corners(){

        corner1x= topLeft.xCoord+sideB;
        corner1y=topLeft.yCoord;
        corner2x=topLeft.xCoord;
        corner2y=topLeft.yCoord-sideA;
        corner3x=topLeft.xCoord+sideB;
        corner3y=topLeft.yCoord-sideA;
        int[][] cornerss={{topLeft.xCoord,topLeft.yCoord},{corner1x,corner1y},{corner3x,corner3y},{corner2x,corner2y}};
        return cornerss;





    }





}
